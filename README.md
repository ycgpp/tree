# tree

#### 介绍
数组转树结构，树结构转数组，根据id和pid进行父子关系处理

#### 安装教程

~~~
composer require sent/tree
~~~

#### 使用说明

~~~
$list = [
    ['id'  => 1, 'parent_id' => 0, 'name' => '一级分类1'],
    ['id'  => 2, 'parent_id' => 1, 'name' => '二级分类1'],
    ['id'  => 3, 'parent_id' => 1, 'name' => '二级分类2'],
    ['id'  => 4, 'parent_id' => 1, 'name' => '二级分类3'],
];

sent\tree\Tree::list_to_tree($list, $root = 0, $pk = 'id', $pid = 'parent_id', $children = 'children')
~~~
